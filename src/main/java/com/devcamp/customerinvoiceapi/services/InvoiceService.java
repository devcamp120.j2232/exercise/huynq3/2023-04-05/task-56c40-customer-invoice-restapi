package com.devcamp.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customerinvoiceapi.models.Invoice;
@Service
public class InvoiceService extends CustomerService{
    Invoice invoice1 = new Invoice(101, customer1, 30000 );
    Invoice invoice2 = new Invoice(102, customer2, 40000 );
    Invoice invoice3 = new Invoice(103, customer3, 50000 );
    public ArrayList<Invoice> getAllInvoices(){
        ArrayList<Invoice> invoiceList = new ArrayList<>();
        invoiceList.add(invoice1);
        invoiceList.add(invoice2);
        invoiceList.add(invoice3);
        return invoiceList;
    }
}
