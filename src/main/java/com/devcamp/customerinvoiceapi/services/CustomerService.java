package com.devcamp.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customerinvoiceapi.models.Customer;
@Service
public class CustomerService {
    Customer customer1 = new Customer(1, "Huy", 10);
    Customer customer2 = new Customer(2, "Yen", 15);
    Customer customer3 = new Customer(3, "Son", 20);
    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        return customerList;
    }
}
